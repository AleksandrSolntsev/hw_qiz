const option1 = document.querySelector('.option1'),
      option2 = document.querySelector('.option2'),
      option3 = document.querySelector('.option3'),
      option4 = document.querySelector('.option4');

/* All our options"*/

const optionElements = document.querySelectorAll('.option');

const question = document.getElementById('question'); // сам вопрос

const numberOfQuestion = document.getElementById('number-of-question'), // номер вопроса
      numberOfAllQuestions = document.getElementById('number-of-all-questions'); // количество всех вопросов
    
let indexOfQuestion, // индекс текущего вопроса 16

    indexOfPage = 0; // индекс страницы

const answersTracker = document.getElementById('answers-tracker'); // обертка для трекера
const btnNext = document.getElementById('btn-next'); //кнопка, далее

let score = 0; // Итоговый результат викторины

const correctAnswer = document.getElementById('correct-answer'),// количест
      numberOfAllQuestions2 = document.getElementById('number-of-all-questions-2'), ///колво вопр.в модальн.окне
      btnTryAgain = document.getElementById('btn-try-again');

const questions = [
    { 
        question: 'Где право?',
        options: [
            'Слева',
            'Справа',
            'С права',
            'Я ёж!',
        ],
        rightAnswer: 1
    },
    { 
        question: 'Как часто нужно бекапить базы?',
        options: [
            'Не нужно бекапить',
            'Раз в месяц',
            'Сделать бекап и сделать бекап бекапа',
            'Я люблю рисковать',
        ],
        rightAnswer: 2
    },
    { 
        question: 'Где зимуют раки?',
        options: [
            'Раки варят, они не доживают до зимы',
            'На Бали',
            'Улетают в теплые края',
            'В пиве',
        ],
        rightAnswer: 0
    },
]      
numberOfAllQuestions.innerHTML = questions.length; //вывод кол-ва вопросов

const load = () => {    //мапим значения в страничку
      question.innerHTML = questions[indexOfQuestion].question;

      //мапим ответы:
      option1.innerHTML = questions[indexOfQuestion].options[0];
      option2.innerHTML = questions[indexOfQuestion].options[1];
      option3.innerHTML = questions[indexOfQuestion].options[2];
      option4.innerHTML = questions[indexOfQuestion].options[3];

      numberOfQuestion.innerHTML = indexOfPage +1;
      indexOfPage++; ///WAT?
};

let completedAnswers = []  //массив для заданых вопросов

const randomQuestion = () => {
      let randomNumber = Math.floor(Math.random() * questions.length);
      let hitDuplicate = false; //якорь

      if(indexOfPage == questions.length) {
          quizOver()
      } else {
          if(completedAnswers.length > 0){
              completedAnswers.forEach(item => {
                  if(item == randomNumber){
                      hitDuplicate = true;
                  }
              });
              if(hitDuplicate){
                  randomQuestion();
             }else {
                 indexOfQuestion = randomNumber;
                 load ();
             }
          }
          if(completedAnswers.length == 0) {
              indexOfQuestion = randomNumber;
              load ();
          }
      }
      completedAnswers.push(indexOfQuestion);
      
};

const checkAnswer = el => {
    if(el.target.dataset.id == questions[indexOfQuestion].rightAnswer) {
        el.target.classList.add('correct');
        updateAnswerTracker('correct');
        score++;
    } else {
        el.target.classList.add('wrong');
        updateAnswerTracker('wrong');
    }
    disabledOptions();


}
for (option of optionElements) {
    option.addEventListener('click' , e => checkAnswer(e));
}

const disabledOptions = () => {
    optionElements.forEach(item => {
        item.classList.add('disabled');
        if(item.dataset.id == questions[indexOfQuestion].rightAnswer){
            item.classList.add('correct');
        }
    })
}

const enableOptions = () => {
    optionElements.forEach(item => {
        item.classList.remove('disabled', 'correct', 'wrong'); ///Удаление всех класов с объектов
    })
}

const answerTracker = () => {
    questions.forEach(() => {
        const div = document.createElement('div');
        answersTracker.appendChild(div);
    })
}

const updateAnswerTracker = status => {
    answersTracker.children[indexOfPage - 1].classList.add(`${status}`)
}

const validate = () => {
    if (!optionElements[0].classList.contains('disabled')){
        alert('Viberi variant');
    } else {
        randomQuestion();
        enableOptions();
}

}
const quizOver = () => {
    document.querySelector('.quiz-over-modal').classList.add('active');
    correctAnswer.innerHTML = score;
    numberOfAllQuestions2.innerHTML = questions.length;
}

const tryAgain = () => {
    window.location.reload ();
};
btnTryAgain.addEventListener('click', tryAgain);

btnNext.addEventListener('click', () => {
    validate();
})
window.addEventListener('load', () => { //прослушиваем событие загрузки страницы и запускаем функцию ЛОАД
    randomQuestion();
    answerTracker();
    
});